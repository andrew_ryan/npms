fn main() {
    use crate::npm::run;
    run();
}
#[allow(warnings)]
pub mod npm{
    pub fn get_html(url:String)->String{
        let mut client = reqwest::blocking::Client::builder().user_agent("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36").build().unwrap();
        client.get(url).timeout(std::time::Duration::from_secs(10)).send().unwrap().text().unwrap()
    }
    pub fn run(){
        let mut arg = doe::args!();
        if arg.len()==0 ||(arg.len()==1&& (&arg[0]=="-h"||&arg[0]=="--help")){
            println!("Cli for search npm packages on terminal");
            println!("just need arguments to search,last argument is page num default page numis 1");
            println!("Example:");
            println!("npms react => search react on npmjs.com");
            println!("npms angular 5 => search angular on npmjs.com get 5 pages");
            return;
        }
        let page_num = if arg.last().is_some(){
            if arg.last().unwrap().parse::<usize>().is_ok(){
                arg.last().unwrap().parse::<usize>().unwrap()
            }else {
                1
            }
        }else{
            1
        };

        use loa::*;
        arg.remove(arg.len()-1);
        let name = arg.join(" ");
        for page in 0..page_num{
            let url = format!("https://www.npmjs.com/search?q={}&page={}&perPage=50",name,page);
            let html = get_html(url);
            let divs = get_elements_by_tag_name(&html, "h3");
            let ps = get_elements_by_tag_name(&html, "p");
            let uls = get_elements_by_tag_name(&html, "ul");
            let uls = get_elements_by_tag_name(&html, "ul");
            let spans = get_elements_by_tag_name(&html, "span");
            let mut a = vec![];
            let mut b = vec![];
            let mut c = vec![];
            let mut d = vec![];
            for div in divs{
                if div.contains("f4"){
                    a.push(div.inner_text().unwrap());
                }
            }
            for div in ps{
                if div.contains("mt1"){
                    b.push(div.inner_text().unwrap());
                }
            }
            for div in uls{
                if div.contains("mv2"){
                    c.push(div.inner_text().unwrap());
                }
            }
            for div in spans{
                if div.contains("Version"){
                    d.push(div.inner_text().unwrap());
                }
            }

            for i in 0..a.len(){
                print!("\n{}\t{}\n{}\n",&a[i],&d[i].split("\n").nth(1).unwrap(),&b[i]);
            }
        }
    }
}