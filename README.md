# Search npm packages on terminal
[![Crates.io](https://img.shields.io/crates/v/npms.svg)](https://crates.io/crates/npms)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/npms)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/npms/-/raw/master/LICENSE)

## Usage
```sh
Cli for search npm packages on terminal
just need arguments to search,last argument is page num default page numis 1
Example:
npms angular 5 => search angular on npmjs.com get 5 pages
npms react => search react on npmjs.com

react   18.2.0
React is a JavaScript library for building user interfaces.

rxjs    7.5.7
Reactive Extensions for modern JavaScript

react-dom       18.2.0
React package for working with the DOM.

react-redux     8.0.5
Official React bindings for Redux

react-dropzone  14.2.3
Simple HTML5 drag-drop zone with React.js
```
## install
 ```sh
 cargo install npms
 ```
